//
//  FaceDetector.cpp
//  OpenCVMacPlugin
//
//  Created by Ihor Konfedrat on 26.03.2023.
//

#include "FaceDetector.hpp"

FaceDetector::FaceDetector(const char* model,
                           const char* config,
                           double input_size_width,
                           double input_size_height,
                           float score_threshold,
                           float nms_threshold,
                           int top_k)
{
    initDetector(model,
                 config,
                 input_size_width,
                 input_size_height,
                 score_threshold,
                 nms_threshold,
                 top_k);
}

Ptr<FaceDetectorYN> FaceDetector::getFaceDetectorYN()
{
    return detector;
}

void FaceDetector::initDetector(const char* model,
                                const char* config,
                                double input_size_width,
                                double input_size_height,
                                float score_threshold,
                                float nms_threshold,
                                int top_k)
{
    detector = FaceDetectorYN::create(model,
                                      config,
                                      Size(input_size_width, input_size_height),
                                      score_threshold,
                                      nms_threshold,
                                      top_k);
}
