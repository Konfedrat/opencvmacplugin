//
//  FaceDetector.hpp
//  OpenCVMacPlugin
//
//  Created by Ihor Konfedrat on 26.03.2023.
//

#ifndef FaceDetector_hpp
#define FaceDetector_hpp

#include <opencv2/objdetect/face.hpp>

using namespace cv;

class FaceDetector
{
public:
    FaceDetector();
    FaceDetector(const char* model,
                 const char* config,
                 double input_size_width,
                 double input_size_height,
                 float score_threshold,
                 float nms_threshold,
                 int top_k);
    Ptr<FaceDetectorYN> getFaceDetectorYN();

private:
    void initDetector(const char* model,
                      const char* config,
                      double input_size_width,
                      double input_size_height,
                      float score_threshold,
                      float nms_threshold,
                      int top_k);
    Ptr<FaceDetectorYN> detector;
};

#endif /* FaceDetector_hpp */
