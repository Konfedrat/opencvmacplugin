//
//  DebugCPP.hpp
//  OpenCVMacPlugin
//
//  Created by Ihor Konfedrat on 21.03.2023.
//

#ifndef DebugCPP_hpp
#define DebugCPP_hpp

#include <string>

/*
 Example:
     Debug::Log("Hellow Red", Color::Red);
     Debug::Log("Hellow Green", Color::Green);
     Debug::Log("Hellow Blue", Color::Blue);
     Debug::Log("Hellow Black", Color::Black);
     Debug::Log("Hellow White", Color::White);
     Debug::Log("Hellow Yellow", Color::Yellow);
     Debug::Log("Hellow Orange", Color::Orange);

     Debug::Log(true, Color::Black);
     Debug::Log(false, Color::Red);
 */

extern "C"
{
    //Create a callback delegate
    typedef void(*FuncCallBack)(const char* message, int color, int size);
    static FuncCallBack callbackInstance = nullptr;
    void RegisterDebugCallback(FuncCallBack cb);
}

//Color Enum
enum class Color { Red, Green, Blue, Black, White, Yellow, Orange };

class  Debug
{
public:
    static void Log(const char* message, Color color = Color::Black);
    static void Log(const std::string message, Color color = Color::Black);
    static void Log(const int message, Color color = Color::Black);
    static void Log(const char message, Color color = Color::Black);
    static void Log(const float message, Color color = Color::Black);
    static void Log(const double message, Color color = Color::Black);
    static void Log(const bool message, Color color = Color::Black);

private:
    static void send_log(const std::stringstream &ss, const Color &color);
};

#endif /* DebugCPP_hpp */
